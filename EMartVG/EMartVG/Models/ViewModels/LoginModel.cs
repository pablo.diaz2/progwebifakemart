﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMartVG.Models.ViewModels
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        
        public string Mensaje { get; set; }
    }
}