﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMartVG.Models;
namespace EMartVG.Controllers
{
    public class ClienteController : Controller
    {
        dbEmart db = new dbEmart();
        // GET: Cliente
        public ActionResult CreaCliente()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreaCliente(Usuarios model)
        {
            var lista = from usr in db.Usuarios
                        where usr.Email == model.Email
                        select usr;
            if (lista.Count() > 0)
            {
                ModelState.AddModelError("Email", "Esta cuenta no se encuentra disponible");
                return View(model);
            }
            else
            {
                model.Id_Estado = 1;
                model.Id_Rol_Usuario = 2;
                //CREA CARRO PARA EL CLIENTE//
                db.Usuarios.Add(model);
                Carro carro = new Carro();
                carro.Id_Usuario = model.Id_Usuario;
                db.Carro.Add(carro);
                db.SaveChanges();

            }
            return Redirect(Url.Content("~/Login/Login/"));
        }

        public ActionResult PerfilCliente()
        {
            if (Session["Usuario"] != null)
            {
                Usuarios usuario = new Usuarios();
                int usrID = int.Parse(Session["Id"].ToString());
                usuario = db.Usuarios.Where(d => d.Id_Usuario == usrID).First();
                return View(usuario);
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }

        [HttpPost]
        public ActionResult PerfilCliente(Usuarios usuario)
        {
            usuario.Id_Rol_Usuario = 2;
            usuario.Id_Estado = 1;
            db.Entry(usuario).State = EntityState.Modified;
            db.SaveChanges();

            return Redirect(Url.Content("~/Home/Index/"));
        }

        public ActionResult HistorialCompras()
        {
            if (Session["Usuario"] != null)
            {
                int usrID = int.Parse(Session["Id"].ToString());
                Detalle_Compra detalle = new Detalle_Compra();
                var lista = db.Detalle_Compra.OrderByDescending(d=> d.Fecha).Where(d => d.Id_Usuario == usrID && d.Id_Estado_Compra==1).ToList();
               
                    return View(lista);
                
            }
            return View();
        }

        public ActionResult HistorialComprasCompletas()
        {
            if (Session["Usuario"] != null)
            {
                int usrID = int.Parse(Session["Id"].ToString());
                Detalle_Compra detalle = new Detalle_Compra();
                var lista = db.Detalle_Compra.OrderByDescending(d => d.Fecha).Where(d => d.Id_Usuario == usrID && d.Id_Estado_Compra == 2).ToList();

                return View(lista);

            }
            return View();
        }
    }
}