﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMartVG.Models;
using EMartVG.Models.ViewModels;

namespace EMartVG.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        dbEmart db = new dbEmart();
        public ActionResult Login()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            var lista = from usr in db.Usuarios
                        where usr.Email == model.Email && usr.Password == model.Password
                        select usr;
            if (lista.Count() > 0)
            {
                Usuarios usuario = lista.First();
                
                if(usuario.Id_Estado==1 && usuario.Id_Rol_Usuario == 1)
                {
                    Session["Admin"] = usuario.Primer_Nombre;
                    Session["AdminID"] = usuario.Id_Usuario;
                    return Redirect(Url.Content("~/Admin/CreaMarcas/"));
                }
                if (usuario.Id_Estado == 1 && usuario.Id_Rol_Usuario == 2)
                {
                    Session["Usuario"] = usuario.Primer_Nombre;
                    Session["Id"] = usuario.Id_Usuario;
                    return Redirect(Url.Content("~/Home/Index/"));
                }
                if (usuario.Id_Estado == 2 )
                {
                    ModelState.AddModelError("Mensaje", "Esta cuenta se encuentra inhabilitada");
                    return View(model);

                }
                
            }
            else
            {
                ModelState.AddModelError("Mensaje", "Verifique su usuario y contraseña");
                return View(model);
            }
            return View();
        }

        //PENDIENTE MATAR SESION
    }
}