﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMartVG.Models;
namespace EMartVG.Controllers
{
    public class NavController : Controller
    {
        dbEmart db = new dbEmart();
        // GET: Nav
        public ActionResult Nav()
        {
            var lista = db.Marca.Where(d => d.Id_Estado_Marca == 1).ToList();
            return PartialView("_Nav", lista);
        }
    }
}