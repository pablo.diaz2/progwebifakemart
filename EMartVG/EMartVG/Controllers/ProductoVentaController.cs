﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMartVG.Models;

namespace EMartVG.Controllers
{
    [OutputCache(VaryByCustom = "*", Duration = 0)]

    public class ProductoVentaController : Controller
    {
        dbEmart db = new dbEmart();
        // GET: ProductoVenta
        public ActionResult DetalleProducto(int id)
        {
            var producto = db.Producto.Find(id);
            return View(producto);
        }

        [HttpPost]
        public ActionResult DetalleProducto(Producto producto)
        {
            if (Session["Usuario"] != null && Session["Id"]!=null)
            {
                
                int usrID = int.Parse(Session["Id"].ToString());
                Carro carro = new Carro();
                carro = db.Carro.Where(d => d.Usuarios.Id_Usuario == usrID).First();


                Orden orden = new Orden();
                var lista = db.Orden.Where(d => d.Id_Producto == producto.Id_Producto);
                if (lista.Count() < 1)
                {
                    orden.Id_Producto = producto.Id_Producto;
                    orden.Id_Carro = carro.Id_Carro;
                    orden.Cantidad = 1;
                    orden.PrecioCantidad = producto.Precio_Producto;
                    db.Orden.Add(orden);
                    db.SaveChanges();

                }               
                return Redirect(Url.Content("~/ProductoVenta/Orden/"));
            }
            return Redirect(Url.Content("~/Login/Login/"));
        }

        public ActionResult Orden()
        {
            if (Session["Usuario"]!=null){
                int usrID = int.Parse(Session["Id"].ToString());
                Carro carro = new Carro();
                carro = db.Carro.Where(d => d.Usuarios.Id_Usuario == usrID).First();
                var lista = db.Orden.ToList().Where(d => d.Id_Carro == carro.Id_Carro);
                return View(lista);
            }
            return Redirect(Url.Content("~/Login/Login/"));
        }
       
        public ActionResult EliminaProductoCarro(int id)
        {
            var orn = db.Orden.Find(id);
            db.Orden.Remove(orn);
            db.SaveChanges();
            return Redirect(Url.Content("~/ProductoVenta/Orden/"));
        }
        
        [HttpPost]
        public ActionResult Orden(FormCollection fc)
        {           
            string[] idOrden = fc.GetValues("Id_Orden");
            string[] cantidad = fc.GetValues("Cantidad");
            string[] precio = fc.GetValues("Precio_Producto");
            
            Orden orden = new Orden();
            if (idOrden != null)
            {
                for (int i = 0; i < idOrden.Length; i++)
                {

                    orden = db.Orden.Find(Convert.ToInt32(idOrden[i]));
                    orden.Cantidad = (double?)Convert.ToDecimal(cantidad[i]);
                    orden.PrecioCantidad = (double?)Convert.ToDecimal(cantidad[i]) * (double?)Convert.ToDecimal(precio[i]);
                    db.Entry(orden).State = EntityState.Modified;
                    db.SaveChanges();

                }
            }          
            return Redirect(Url.Content("~/ProductoVenta/Orden/"));
        }
        //DETALLE COMPRA

        public ActionResult Crea_DetalleCompra()
        {
            if (Session["Usuario"] != null)
            {
                //CHAYA
                int usrID = int.Parse(Session["Id"].ToString());
                Producto producto = new Producto();
                Carro carro = new Carro();
                carro = db.Carro.Where(d => d.Usuarios.Id_Usuario == usrID).First();
                Orden orden = new Orden();
                var lista = db.Orden.Where(d => d.Id_Carro == carro.Id_Carro);
                Detalle_Compra detalle = new Detalle_Compra();

                string detalleC = "";
                double? valorTotal = 0;
                int dispValorTotal = 0;
                if (lista.Count() > 0)
                {
                    foreach (var item in lista)
                    {
                        //Modifica stock de productos
                        producto = db.Producto.Where(d => d.Id_Producto == item.Id_Producto).First();
                        producto.Stock_Producto -= (int)item.Cantidad;
                        db.Entry(producto).State = EntityState.Modified;
                        //Crea string con historail de compras
                        int dispCant = (int)item.Cantidad;
                        int disPrecioCant = (int)item.PrecioCantidad;
                        detalleC += item.Producto.Nombre_Producto + " x " + item.Cantidad + " Sub total: " + disPrecioCant.ToString("C") + "#";
                        valorTotal += item.PrecioCantidad;
                        dispValorTotal += disPrecioCant;
                    }

                   //Almacena historial de compra
                    detalleC = detalleC + "Valor compra: " + dispValorTotal.ToString("C");
                    detalle.Id_Usuario = usrID;
                    detalle.Id_Estado_Compra = 1;
                    //detalle.Id_Orden
                    detalle.Detalle = detalleC;
                    detalle.Fecha = System.DateTime.Now;
                    db.Detalle_Compra.Add(detalle);

                    //Remueve productos del carro
                    foreach (var item in lista)
                    {
                        db.Orden.Remove(item);
                    }
                    //Guarda Todos los cambios
                    db.SaveChanges();
                }
            }
           
            return View();
        }
    }
}