﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMartVG.Models;
using PagedList.Mvc;
using PagedList;

namespace EMartVG.Controllers
{
    public class HomeController : Controller
    {
        dbEmart db = new dbEmart();

        //PENDIENTE -> VISTAS PARCIALES PAGINACION, LISTAR EN BASE A ESTADO DE PRODUCTOS,
        //VISTA PARCIAL DESPLEGABLE CARRO

        public ActionResult Index(int? i)
        {
            Session["nom"] = null;
            Session["nom2"] = null;
            Session["nomCat"] = null;
            var lista = db.Producto.OrderBy(d => d.Id_Estado_Producto).Where(d => d.Id_Estado_Producto == 1 && d.Marca.Id_Estado_Marca == 1).ToList().ToPagedList(i ?? 1, 9);
            return View(lista);
        }
        public ActionResult Tienda(int? i, string nomMarca)
        {
            if (nomMarca != null)
            {
                Session["nom"] = nomMarca;              
            }
            if (Session["nom"] != null)
            {
                string nomM = Session["nom"].ToString();
                var lista = db.Producto.OrderBy(d=> d.Id_Estado_Producto).Where(d => d.Marca.Nombre_Marca == nomM &&
                d.Id_Estado_Producto == 1).ToList().ToPagedList(i ?? 1, 9);
                return View(lista);
            }
            //No hay msje de error así que al index nomas =P
            return Redirect(Url.Content("~/Home/Index/"));
        }
        public ActionResult Categoria(int? i, string nomMarca2, string categoria)
        {

            if (nomMarca2 != null )
            {
                Session["nom2"] = nomMarca2;
            }
            if (categoria != null)
            {
                Session["nomCat"] = categoria;
            }
            if (Session["nom2"] != null && Session["nomCat"] != null)
            {
                string nomM = Session["nom2"].ToString();
                string nomCat = Session["nomCat"].ToString();
                var lista = db.Producto.OrderBy(d => d.Nombre_Producto).Where(d => d.Marca.Nombre_Marca == nomM && d.Categorias.Nombre_Categoria==nomCat && d.Id_Estado_Producto==1).ToList().ToPagedList(i ?? 1, 9);
                return View(lista);
            }
            else
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }

            
        }

      


    }
}