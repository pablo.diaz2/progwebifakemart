﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMartVG.Models;
using PagedList.Mvc;
using PagedList;


namespace EMartVG.Controllers
{
    public class AdminController : Controller
    {
        
        dbEmart db = new dbEmart();
        
        // GET: Admin

        //ADMINISTRADOR
        public ActionResult CreaAdmin()
        {
            if (Session["Admin"] != null)
            {
                //sk.ListaProductoKill();
                return View();
            }
            return Redirect(Url.Content("~/Home/Index/"));

        }
        [HttpPost]
        public ActionResult CreaAdmin(Usuarios model)
        {
            var lista = from usr in db.Usuarios
                        where usr.Email == model.Email
                        select usr;
            if (lista.Count() > 0)
            {
                ModelState.AddModelError("Email", "Esta cuenta no se encuentra disponible");
                return View(model);
            }
            else
            {
                model.Id_Estado = 1;
                model.Id_Rol_Usuario = 1;
                db.Usuarios.Add(model);
                db.SaveChanges();
                return View();
            }

        }
        //EDITA ADMIN (Logeado)
        public ActionResult EditaAdmin()
        {
            if (Session["Admin"] != null)
            {

                Usuarios usuario = new Usuarios();
                //ViewBag.Id_Estado = new SelectList(db.Estado_Usuarios, "Id_Estado", "Estado"); //recordar esto =P
                int usrID = int.Parse(Session["AdminID"].ToString());
                usuario = db.Usuarios.Where(d => d.Id_Usuario == usrID).First();
                return View(usuario);
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }

        //EDITA ADMIN (Logeado)
        [HttpPost]
        public ActionResult EditaAdmin(Usuarios usuario)
        {
            //ViewBag.Id_Estado = new SelectList(db.Estado_Usuarios, "Id_Estado", "Estado"); //recordar esto =P
            usuario.Id_Rol_Usuario = 1;
            usuario.Id_Estado = 1;
            db.Entry(usuario).State = EntityState.Modified;
            db.SaveChanges();
            if (usuario.Id_Estado == 2)
            {
                Session["Admin"] = null;
                return Redirect(Url.Content("~/Home/Index/"));
            }
            else
            {
                return Redirect(Url.Content("~/Home/Index/")); //Cambiar vista cuando este lista
            }
        }

        //FIN ADMINISTRADOR

        //MARCAS
        public ActionResult CreaMarcas()
        {
            if (Session["Admin"] != null)
            {
                return View();
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }
        [HttpPost]
        public ActionResult CreaMarcas(Marca marca)
        {
            var lista = from mar in db.Marca
                        where mar.Nombre_Marca == marca.Nombre_Marca
                        select mar;
            if (lista.Count() > 0)
            {
                ModelState.AddModelError("Nombre_Marca", "Esta marca ya existe");
                return View(marca);
            }
            marca.Id_Estado_Marca = 1;
            db.Marca.Add(marca);
            db.SaveChanges();
            return View();
        }

        public ActionResult ListarMarcas()
        {

            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            ViewBag.estadomarca = new SelectList(db.Estado_Marca, "Id_Estado_Marca", "Estado_Marca1");
            return View();
        }
        //BUSCAR CON FILTROS MARCA
        public ActionResult ListaMarcaPartial(string nom, int? estado, int? i)
        {

            if (nom != "" && estado != null)
            {
                var lista = db.Marca.Where(d => d.Nombre_Marca.Contains(nom) && d.Id_Estado_Marca == estado).ToList().ToPagedList(i ?? 1, 9);
                return PartialView("_ListaMarcaPartial", lista);
            }
            if (nom != "" && estado == null)
            {
                var lista = db.Marca.Where(d => d.Nombre_Marca.Contains(nom)).ToList().ToPagedList(i ?? 1, 9);
                return PartialView("_ListaMarcaPartial", lista);
            }
            if (nom == "" && estado != null)
            {
                var lista = db.Marca.Where(d => d.Id_Estado_Marca == estado).ToList().ToPagedList(i ?? 1, 9);
                return PartialView("_ListaMarcaPartial", lista);
            }
            else
            {
                var lista = db.Marca.ToList().ToPagedList(i ?? 1, 9);
                return PartialView("_ListaMarcaPartial", lista);
            }

        }
        //FIN FILTRO NOMBRE MARCA
        public ActionResult EditarMarca(int id)
        {
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            var marca = db.Marca.Find(id);
            return View(marca);
        }

        [HttpPost]
        public ActionResult EditarMarca(Marca marca)
        {
            db.Entry(marca).State = EntityState.Modified;
            db.SaveChanges();
            return Redirect(Url.Content("~/Admin/ListarMarcas"));
        }

        //CAMBIA ESTADO DE MARCAS
        public ActionResult EstadoMarca(int id)
        {
            var marca = db.Marca.Find(id);
            if (marca.Id_Estado_Marca == 1)
            {
                marca.Id_Estado_Marca = 2;
                db.Entry(marca).State = EntityState.Modified;

            }
            else
            {
                marca.Id_Estado_Marca = 1;
                db.Entry(marca).State = EntityState.Modified;
            }
            db.SaveChanges();
            return Redirect(Url.Content("~/Admin/ListarMarcas"));
        }

        //FIN MARCAS

        //CATEGORIAS
        public ActionResult CreaCategoria()
        {
            if (Session["Admin"] != null)
            {
                return View();
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }

        [HttpPost]
        public ActionResult CreaCategoria(Categorias categoria)
        {
            var lista = from cat in db.Categorias
                        where cat.Nombre_Categoria == categoria.Nombre_Categoria
                        select cat;
            if (lista.Count() > 0)
            {
                ModelState.AddModelError("Nombre_Categoria", "Esta categoría ya existe");
                return View(categoria);
            }
            categoria.Id_Estado_Categorias = 1;
            db.Categorias.Add(categoria);
            db.SaveChanges();
            return View();
        }

        public ActionResult ListarCategorias()
        {
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            ViewBag.estadocategoria = new SelectList(db.Estado_Categorias, "Id_Estado_Categoria", "Estado_Categoria");
            return View();
        }

        //BUSCAR CON FILTRO CATEGORIA
        public ActionResult ListaCategoriaPartial(string nom, int? estado)
        {
            if (nom != "" && estado != null)
            {
                var lista = db.Categorias.Where(d => d.Nombre_Categoria.Contains(nom) && d.Id_Estado_Categorias == estado);
                return PartialView("_ListaCategoriaPartial", lista);
            }
            if (nom != "" && estado == null)
            {
                var lista = db.Categorias.Where(d => d.Nombre_Categoria.Contains(nom));
                return PartialView("_ListaCategoriaPartial", lista);
            }
            if (nom == "" && estado != null)
            {
                var lista = db.Categorias.Where(d => d.Id_Estado_Categorias == estado).ToList();
                return PartialView("_ListaCategoriaPartial", lista);
            }
            else
            {
                var lista = db.Categorias.ToList();
                return PartialView("_ListaCategoriaPartial", lista);
            }

        }

        public ActionResult EditarCategoria(int id)
        {
            if (Session["Admin"] != null)
            {
                var categoria = db.Categorias.Find(id);
                return View(categoria);
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }

        [HttpPost]
        public ActionResult EditarCategoria(Categorias categoria)
        {
            //categoria.Id_Estado_Categorias = 1;
            db.Entry(categoria).State = EntityState.Modified;
            db.SaveChanges();
            return Redirect(Url.Content("~/Admin/ListarCategorias"));
        }

        public ActionResult EstadoCategoria(int id)
        {
            var categorias = db.Categorias.Find(id);
            if (categorias.Id_Estado_Categorias == 1)
            {
                categorias.Id_Estado_Categorias = 2;
                db.Entry(categorias).State = EntityState.Modified;

            }
            else
            {
                categorias.Id_Estado_Categorias = 1;
                db.Entry(categorias).State = EntityState.Modified;
            }
            db.SaveChanges();
            return Redirect(Url.Content("~/Admin/ListarCategorias"));
        }

        //FIN CATEGORIA

        //PRODUCTO//
        public ActionResult CreaProducto()
        {
            if (Session["Admin"] != null)
            {

                ViewBag.Id_Marca = new SelectList(db.Marca, "Id_Marca", "Nombre_Marca"); //recordar esto =P
                ViewBag.Id_Categoria = new SelectList(db.Categorias, "Id_Categoria", "Nombre_Categoria"); //recordar esto =P
                return View();
            }
            return Redirect(Url.Content("~/Home/Index/"));

        }

        [HttpPost]
        public ActionResult CreaProducto(Producto producto, HttpPostedFileBase Imagen_Producto)
        {
            ViewBag.Id_Marca = new SelectList(db.Marca, "Id_Marca", "Nombre_Marca");
            ViewBag.Id_Categoria = new SelectList(db.Categorias, "Id_Categoria", "Nombre_Categoria");

            
            var lista = db.Producto.Where(d => d.Nombre_Producto == producto.Nombre_Producto && d.Id_Marca == producto.Id_Marca).ToList();
            if (lista.Count() > 0)
            {
                ModelState.AddModelError("Nombre_Producto", "Este Producto ya existe");
                return View(producto);
            }
            else
            {
                string filename = "";
                if (producto.Imagen_Producto != null)
                {
                    filename = Path.GetFileNameWithoutExtension(Imagen_Producto.FileName);
                    string extension = Path.GetExtension(Imagen_Producto.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                    producto.Imagen_Producto = "~/Assets/ImgProductos/" + filename;
                    filename = Path.Combine(Server.MapPath("~/Assets/ImgProductos/"), filename);
                    Imagen_Producto.SaveAs(filename);
                }


                producto.Id_Estado_Producto = 1;
                db.Producto.Add(producto);
                db.SaveChanges();

                SuvNavCategorias sub = new SuvNavCategorias();
                sub.Id_Categoria = producto.Id_Categoria;
                sub.Id_Marca = producto.Id_Marca;

                var lista2 = db.SuvNavCategorias.Where(d => d.Id_Categoria == producto.Id_Categoria && d.Id_Marca == producto.Id_Marca).ToList();
                if (lista2.Count() < 1)
                {
                    db.SuvNavCategorias.Add(sub);
                    db.SaveChanges();
                }
                
                return View();
            }

        }

        public ActionResult ListarProductos()
        {
            if (Session["Admin"] != null)
            {
                
                ViewBag.estadoproducto = new SelectList(db.Estado_Productos, "Id_Estado_Producto", "Estado_Producto");                
                ViewBag.marcaproducto = new SelectList(db.Marca, "Id_Marca", "Nombre_Marca");
                ViewBag.categoriaproducto = new SelectList(db.Categorias,"Id_Categoria","Nombre_Categoria");
                return View();
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }

        public ActionResult ListarProductosPartial(string nom, int? estado, int? marca, int? categoria )
        {
            //PENDIENTE AGREGAR MAS VARIABLES DE SESION
            //Session["nomproducto"] = nom;
            //Session["estadoproducto"] = estado;
            //Session["marcaproducto"] = marca;
            //Session["categoriaproducto"] = categoria;


            //BUSQUEDA POR NOMBRE, CATEGORIA, ESTADO Y MARCA (funciona)1
            if (nom != "" && estado != null && categoria!=null && marca!=null)
            {
                var lista = db.Producto.Where(d => d.Nombre_Producto.Contains(nom) && d.Id_Estado_Producto == estado 
                && d.Id_Categoria==categoria && d.Id_Marca==marca).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //BUSQUEDA POR CATEGORIA, ESTADO Y MARCA (funciona)2
            if (nom == "" && estado != null && categoria != null && marca != null)
            {
                var lista = db.Producto.Where(d => d.Id_Estado_Producto == estado
                && d.Id_Categoria == categoria && d.Id_Marca == marca).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //NOMBRE MARCA Y CATEGORIA(funciona)3
            if (nom != "" && estado == null && categoria != null && marca != null)
            {
                var lista = db.Producto.Where(d => d.Nombre_Producto.Contains(nom) && 
                d.Id_Categoria == categoria && d.Id_Marca == marca).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }

            //NOMBRE ESTADO Y CATEGORIA(funciona)4
            if (nom != "" && estado != null && categoria != null && marca == null)
            {
                var lista = db.Producto.Where(d => d.Nombre_Producto.Contains(nom) && 
                d.Id_Categoria == categoria && d.Id_Estado_Producto == estado).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }

            //NOMBRE ESTADO Y MARCA(funciona)5
            if (nom != "" && estado != null && categoria == null && marca != null)
            {
                var lista = db.Producto.Where(d => d.Nombre_Producto.Contains(nom) &&
                d.Id_Estado_Producto == estado && d.Id_Marca==marca).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }

            //SOLO POR CATEGORIA (funciona)6
            if (nom == "" && estado == null && categoria != null && marca == null)
            {
                var lista = db.Producto.Where(d => d.Id_Categoria == categoria).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //BUSQUEDA POR CATEGORIA Y NOMBRE (funciona)7
            if (nom != "" && estado == null && categoria != null && marca == null)
            {
                var lista = db.Producto.Where(d => d.Id_Categoria == categoria && 
                d.Nombre_Producto.Contains(nom)).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //BUSQUEDA POR CATEGORYA Y MARCA (funciona)8
            if (nom == "" && estado == null && categoria != null && marca != null)
            {
                var lista = db.Producto.Where(d => d.Id_Categoria == categoria && 
                d.Id_Marca == marca).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //BUSQUEDA POR CATEGORIA Y ESTADO (funciona)9
            if (nom == "" && estado != null && categoria != null && marca == null)
            {
                var lista = db.Producto.Where(d => d.Id_Categoria == categoria &&
                d.Id_Estado_Producto == estado).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }

            //SOLO POR NOMBRE(funciona)10
            if (nom!=null && estado == null && categoria == null && marca == null)
            {
                var lista = db.Producto.Where(d => d.Nombre_Producto.Contains(nom)).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //NOMBRE Y ESTADO(funciona)11
            if (nom != null && estado != null && categoria == null && marca == null)
            {
                var lista = db.Producto.Where(d => d.Nombre_Producto.Contains(nom)&&
                d.Id_Estado_Producto==estado).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //NOMBRE Y MARCA(funciona)12
            if (nom != null && estado == null && categoria == null && marca != null)
            {
                var lista = db.Producto.Where(d => d.Nombre_Producto.Contains(nom) && 
                d.Id_Marca == marca).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //SOLO POR ESTADO(funciona)13
            if (nom == "" && estado != null && categoria == null && marca == null)
            {
                var lista = db.Producto.Where(d => d.Id_Estado_Producto==estado).ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
            //ESTADO Y MARCA(funciona)14
            if (nom == "" && estado != null && categoria == null && marca != null)
            {
                var lista = db.Producto.Where(d => d.Id_Marca == marca && 
                d.Id_Estado_Producto==estado);
                return PartialView("_ListarProductosPartial", lista);
            }

            //SOLO POR MARCA(funciona)15
            if (nom == "" && estado == null && categoria == null && marca != null)
            {
                var lista = db.Producto.Where(d => d.Id_Marca == marca);
                return PartialView("_ListarProductosPartial", lista);
            }
            else
            {
                var lista = db.Producto.ToList();
                return PartialView("_ListarProductosPartial", lista);
            }
        }

        public ActionResult EditarProducto(int id)
        {

            if (Session["Admin"] != null)
            {
                ViewBag.Id_Marca = new SelectList(db.Marca, "Id_Marca", "Nombre_Marca");
                ViewBag.Id_Categoria = new SelectList(db.Categorias, "Id_Categoria", "Nombre_Categoria");
                var producto = db.Producto.Find(id);
                return View(producto);
            }

            return Redirect(Url.Content("~/Home/Index/"));
        }

        [HttpPost]
        public ActionResult EditarProducto(Producto producto, HttpPostedFileBase Imagen)
        {

            ViewBag.Id_Marca = new SelectList(db.Marca, "Id_Marca", "Nombre_Marca");
            ViewBag.Id_Categoria = new SelectList(db.Categorias, "Id_Categoria", "Nombre_Categoria");

            string path = Server.MapPath(producto.Imagen_Producto);
            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Delete(path);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);

                }
            }

            string filename = Path.GetFileNameWithoutExtension(Imagen.FileName);
            string extension = Path.GetExtension(Imagen.FileName);
            filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
            producto.Imagen_Producto = "~/Assets/ImgProductos/" + filename;
            filename = Path.Combine(Server.MapPath("~/Assets/ImgProductos/"), filename);
            Imagen.SaveAs(filename);


            //producto.Id_Estado_Producto = 1;
            db.Entry(producto).State = EntityState.Modified;


            db.SaveChanges();
            return Redirect(Url.Content("~/Admin/ListarProductos"));
            
        }

        public ActionResult EstadoProducto(int id)
        {
            var producto = db.Producto.Find(id);
            if (producto.Id_Estado_Producto == 1)
            {
                producto.Id_Estado_Producto = 2;
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                producto.Id_Estado_Producto = 1;
                db.Entry(producto).State = EntityState.Modified;

                SuvNavCategorias sub = new SuvNavCategorias();
                sub.Id_Categoria = producto.Id_Categoria;
                sub.Id_Marca = producto.Id_Marca;

                var lista2 = db.SuvNavCategorias.Where(d => d.Id_Categoria == producto.Id_Categoria && d.Id_Marca == producto.Id_Marca).ToList();
                if (lista2.Count() < 1)
                {
                    db.SuvNavCategorias.Add(sub);
                    db.SaveChanges();
                }
            }
            db.SaveChanges();
            return Redirect(Url.Content("~/Admin/ListarProductos"));
        }
        
        //FIN PRODUCTO

        //ORDENES
        public ActionResult ListarVentas()
        {
            if (Session["Admin"] != null)
            {
                ViewBag.estadoventa = new SelectList(db.Estado_Compras, "Id_Estado_Compra", "Estado_Compra");
                return View();
            }
            return Redirect(Url.Content("~/Home/Index"));
        }

        public ActionResult ListarVentasPartial(string nom, int? estado)
        {

            if (nom != "" && estado != null)
            {
                var lista = db.Detalle_Compra.OrderByDescending(d=>d.Fecha).Where(d => d.Id_Detalle_Compra.ToString()==nom && d.Id_Estado_Compra == estado);
                return PartialView("_ListarVentasPartial", lista);
            }
            if (nom != "" && estado == null)
            {
                var lista = db.Detalle_Compra.OrderByDescending(d => d.Fecha).Where(d => d.Id_Detalle_Compra.ToString()==nom);
                return PartialView("_ListarVentasPartial", lista);
            }
            if (nom == "" && estado != null)
            {
                var lista = db.Detalle_Compra.OrderByDescending(d => d.Fecha).Where(d => d.Id_Estado_Compra == estado).ToList();
                return PartialView("_ListarVentasPartial", lista);
            }
            else
            {
                var lista = db.Detalle_Compra.OrderByDescending(d=>d.Fecha).ToList();
                return PartialView("_ListarVentasPartial", lista);
            }
        }
        public ActionResult AprobarVenta(int id)
        {
            var venta = db.Detalle_Compra.Find(id);
            return View(venta);
        }

        [HttpPost]
        public ActionResult AprobarVenta(Detalle_Compra det)
        {
            var lista = db.Detalle_Compra.Where(d => d.Codigo_Envio == det.Codigo_Envio).ToList();
            if (lista.Count > 0)
            {
                ModelState.AddModelError("Codigo_Envio", "Este código ya ha sido ingresado");
                return View(det);
            }
            else
            {
                if (det.Id_Estado_Compra == 1)
                {
                    det.Id_Estado_Compra = 2;
                }
                db.Entry(det).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Redirect(Url.Content("~/Admin/ListarVentas"));
        }
        //FIN ORDENES
    }
}